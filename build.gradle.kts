plugins {
    id("com.android.library")
    kotlin("multiplatform") version "1.4.32"
    kotlin("plugin.serialization") version "1.4.32"
    id("maven-publish")
}

group = "me.kacper.barszczewski"
version = "1.3.6-SNAPSHOT"

val serializationVersion = "1.1.0"
val coroutinesVersion = "1.4.3"

repositories {
    google()
    mavenCentral()
    jcenter()
    maven("https://dl.bintray.com")
}

kotlin {
    jvm {
//        withJava()
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
    }
    android()
    sourceSets {
//        val commonMain by getting
//        val commonTest by getting
        val commonJvmAndroid = create("commonJvmAndroid") {
//            dependsOn(commonMain)
            dependencies {
                implementation("com.github.aakira:napier:1.5.0-alpha1")

                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$serializationVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$serializationVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
//                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:0.20.0")
            }
        }
        val commonJvmAndroidTest = create("commonJvmAndroidTest") {
            dependsOn(commonJvmAndroid)
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutinesVersion")
            }
        }
        val jvmMain by getting {
            dependsOn(commonJvmAndroid)
//            kotlin.srcDir("src/jvmMain/kotlin")
        }
        val jvmTest by getting {
            dependsOn(commonJvmAndroidTest)
            dependsOn(jvmMain)
//            kotlin.srcDir("src/jvmMain/kotlin")
            dependencies {
            }
        }
        val androidMain by getting {
//            kotlin.srcDir("src/commonJvmAndroid/kotlin")
            dependsOn(commonJvmAndroid)
            dependencies {
                implementation("com.google.android.material:material:1.3.0")
            }
        }
        val androidTest by getting {
            dependsOn(commonJvmAndroidTest)
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("junit:junit:4.13")
            }
        }
    }

    android {
        publishLibraryVariantsGroupedByFlavor = true
        publishLibraryVariants("release", "debug")
    }
}

android {
    compileSdkVersion(29)
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
//        applicationId = "me.kacper.barszczewski.library"
        minSdkVersion(22)
        targetSdkVersion(29)
    }
    buildFeatures {
        viewBinding = true
    }
}