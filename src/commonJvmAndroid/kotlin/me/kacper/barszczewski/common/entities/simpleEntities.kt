package me.kacper.barszczewski.common.entities

import kotlinx.serialization.Serializable
import me.kacper.barszczewski.common.Message
import me.kacper.barszczewski.common.servers.FileExistsReturnType
import me.kacper.barszczewski.common.servers.FileStatusMessage
import me.kacper.barszczewski.common.servers.FileTransferStatus
import me.kacper.barszczewski.common.servers.FileTransferType
import java.lang.RuntimeException


@Serializable
data class FileChunkEntity(
    var data: ByteArray,
    var chunkLength: Long,
    override var index: Long
) : MessageEntity(), IndexableFileEntity {
    override fun code(): String {
        return Message.FILE_CHUNK
    }
}

@Serializable
data class FileStatusEntity(
    var status: FileTransferStatus,
    override var index: Long,
    var messageCode: FileStatusMessage? = null
) : MessageEntity(), IndexableFileEntity {
    override fun code(): String {
        return Message.FILE_STATUS
    }
}

@Serializable
data class FileTransferEntity(
    var filePath: String,
    var fileLength: Long,
    var transferType: FileTransferType,
    override var index: Long
) : MessageEntity(), IndexableFileEntity {
    override fun code(): String {
        return Message.FILE_TRANSFER
    }
}

@Serializable
data class TransferInitializationEntity(
//    val hostname: String,
//    val socketPort: Int,
//    val filesCount: Int,
    val filePath: List<String>,
    val transferType: FileTransferType,
    val destPath: String
) : MessageEntity() {
    override fun code(): String {
        return Message.FILE_INITIALIZE
    }
}

@Serializable
data class FileOverridePolicyEntity(
    val file: String,
    var fileExistsPolicy: FileExistsReturnType? = null,
    override val index: Long
) : MessageEntity(), IndexableFileEntity {
    override fun code(): String {
        return Message.FILE_OVERR_POLICY
    }
}

@Serializable
data class FileConnectionEntity(
    val hostname: String,
    val socketPort: Int
): MessageEntity() {
    override fun code(): String {
        return Message.FILE_CONNECTION
    }
}

interface IndexableFileEntity {
    val index: Long
}

@Serializable
abstract class MessageEntity {
    abstract fun code(): String
}