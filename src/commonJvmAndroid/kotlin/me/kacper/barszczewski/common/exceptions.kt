package me.kacper.barszczewski.common

import java.lang.Exception

open class InputClosedException(message: String) : Exception(message)

open class TransferClosedByClient(message: String) : CloseClientWithException(message)

open class CloseClientWithException(message: String) : Exception(message)