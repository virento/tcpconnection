package me.kacper.barszczewski.common

//import mu.KotlinLogging
import me.kacper.barszczewski.common.entities.FileChunkEntity
import me.kacper.barszczewski.common.entities.FileTransferEntity
import me.kacper.barszczewski.common.entities.MessageEntity
import me.kacper.barszczewski.common.servers.FileClient
import me.kacper.barszczewski.common.servers.FileTransferType
import me.kacper.barszczewski.common.utils.LoggerWrapper
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.nio.ByteBuffer

fun RemoteClient.sendProperty(message: String) {
    sendProperty(message.toByteArray())
}

fun <T> RemoteClient.sendProperty(pair: Pair<String, T>, separator: String = ";") {
    sendProperty("${pair.first}:${pair.second.toString()}$separator")
}

fun RemoteClient.sendProperty(message: ByteArray) {
    val output = outputStream()
    output.write(message)
}

fun RemoteClient.sendMessage(code: String, content: ByteArray) {
    val output = outputStream()
    val msgSizeLength =
        (properties[MSG_SIZE_LENGTH] ?: throw RuntimeException("Property $MSG_SIZE_LENGTH not set")).toInt()
    val msgCodeLength =
        (properties[MSG_MESSAGE_LENGTH] ?: throw RuntimeException("Property $MSG_MESSAGE_LENGTH not set")).toInt()
    val msgCode = toFixedByteLength(code, msgCodeLength)
    val msgLength = toFixedByteLength(content.size.toString(), msgSizeLength)
    val message = ByteBuffer.allocate(msgCodeLength + msgSizeLength + content.size)
        .put(msgCode)
        .put(msgLength)
        .put(content)
        .array()
    LoggerWrapper.debug(this::class) {
        "Send: ${client.localAddress} to ${client.inetAddress}. Message '$code'. Content as String: '${
            String(
                content
            )
        }'"
    }
    output.write(message)

}

fun RemoteClient.writeStreamFile(
//    outputStream: OutputStream,
    input: InputStream,
    fileTransferEntity: FileTransferEntity,
    chunkRead: Int = 2048,
    index: Long
) {
    var available = input.available()
    var written = 0L
//    val fileLength = fileTransferEntity.fileLength
    while (available > 0) {
        if (available > chunkRead) available = chunkRead
//        if (written + available > fileLength) available = (fileLength - written).toInt()
        val bytes = ByteArray(available)
        val read = input.read(bytes)
        sendMessage(FileChunkEntity(bytes, read.toLong(), index))
//        outputStream.write(bytes, 0, read)
        written += read
        available = input.available()
    }
}

//fun FileClient.sendFile(file: File, destPath: String, chunkRead: Int = 2048, index: Int) {
//    val out = outputStream()
//
//    //TODO: try catch?
//    //SecurityManager().checkRead(file.canonicalPath)
//
////        TODO: File.name czy na pewno?
//    sendMessage(FileTransferEntity(destPath, file.length(), FileTransferType.SERVER_TO_CLIENT, index))
//
//    var startSending = false
//    do {
//        val message = readMessage()
//
//        //TODO: read message and invoke listeners?
//        when(message.message) {
//            MSG_IS_GOOD -> startSending = true
//            MSG_SKIP -> {
//                logger.debug {"Skipping file $file"}
//                return
//            }
//        }
//        if (message.message == MSG_IS_GOOD) {
//            startSending = true
//        }
//    } while (!startSending)
//
//    val input = file.inputStream()
//    input.use { inputStream ->
//        val array = ByteArray(chunkRead)
//        var read = inputStream.read(array)
//        while (read != -1) {
//            out.write(array, 0, read)
//            read = inputStream.read(array)
//        }
//    }
//    input.close()
//    logger.debug { "File '$file' has been sent" }
//}

/**
 * file - File from client, should not be used to obtain input stream/output stream
 * saveFile - File from which server should obtain output stream to write into
 */
//fun FileClient.readFile(file: File, saveFile: File, chunkRead: Int = 2048, index: Int) {
//    sendMessage(FileTransferEntity(file.canonicalPath, -1L, FileTransferType.CLIENT_TO_SERVER, index))
//
//    var preceed = false
//    var fileTransferEntityMessage: FileTransferEntity? = null
//    do {
//        val message = readMessage()
//        if (message.message == Message.FILE_TRANSFER)  {
//            fileTransferEntityMessage = message.deserialize()
//            preceed = true
//        }
//    } while (!preceed)
//    fileTransferEntityMessage ?: throw RuntimeException("File transfer message not received")
//    fileTransferEntityMessage.filePath = saveFile.path
//
//    checkOverridePolicy(fileTransferEntityMessage)
//    if (shouldSkip) {
//        sendMessage(MSG_SKIP)
//        return
//    }
//    sendMessage(MSG_IS_GOOD)
//    val input = inputStream()
//    val out = saveFile.outputStream()
//    out.use { outputStream -> writeStreamFile(outputStream, input, fileTransferEntityMessage, chunkRead) }
//}

fun RemoteClient.sendMessage(code: String) {
    val output = outputStream()
    val msgCodeLength =
        (properties[MSG_MESSAGE_LENGTH] ?: throw RuntimeException("Property $MSG_MESSAGE_LENGTH not set")).toInt()
    val msgSizeLength =
        (properties[MSG_SIZE_LENGTH] ?: throw RuntimeException("Property $MSG_SIZE_LENGTH not set")).toInt()
    val message = ByteBuffer.allocate(msgCodeLength + msgSizeLength)
        .put(toFixedByteLength(code, msgCodeLength))
        .put(toFixedByteLength("0", msgSizeLength))
        .array()
    output.write(message)
    LoggerWrapper.debug(this::class) { "Send: ${client.localAddress} to ${client.inetAddress}. Message '$code'" }
}

fun RemoteClient.sendMessage(messageEntity: MessageEntity) {
    sendMessage(messageEntity.code(), MessageFactory.serialize(messageEntity).toByteArray())
}

fun RemoteClient.sendMessageAndRun(messageEntity: MessageEntity, run: Runnable) {
    run()
    sendMessage(messageEntity)
}

fun toFixedByteLength(text: String, length: Int): ByteArray {
    var fixedText = text
    if (fixedText.length > length) {
        fixedText = fixedText.substring(0, length)
    }
    val array = ByteArray(length)
    fixedText.toByteArray().copyInto(array)
    return array
}

// TODO: 09.03.2021 replace available, risk of buffer overflow
fun RemoteClient.readAvailable(): ByteArray {
    val input = inputStream()
    val read = input.read()
    if (read == -1) { //End of stream
        throw InputClosedException("End of input stream")
    }
    val available = input.available()
    val readByte = read.toByte()
    if (available == 0) {
        return byteArrayOf(readByte)
    }
    val byteArray = ByteArray(available + 1)
    byteArray[0] = readByte
    input.read(byteArray, 1, available)
    return byteArray
}

fun RemoteClient.readMessages(): List<MessageData> {
    val message = readAvailable()
    val messages = mutableListOf<MessageData>()
    var index = 0
    while (index < message.size) {
        val messageData = getMessage(message, index)
        index += messageData.totalLength
        messages.add(messageData)
    }
    return messages.toList()
}

inline fun <reified T : MessageEntity> RemoteClient.readEntity(): T {
    return readMessage().deserialize()
}

fun RemoteClient.readMessage(): MessageData {
    val input = inputStream()
    val msgSizeLength =
        (properties[MSG_SIZE_LENGTH] ?: throw RuntimeException("Property $MSG_SIZE_LENGTH not set")).toInt()
    val msgCodeLength =
        (properties[MSG_MESSAGE_LENGTH] ?: throw RuntimeException("Property $MSG_MESSAGE_LENGTH not set")).toInt()
    val message = ByteArray(msgSizeLength + msgCodeLength)
    readWithSize(input, message, msgSizeLength + msgCodeLength)
    var index = 0

    val msgCode = message.copyOfRange(index, index + msgCodeLength).stringTrimZeros()
    index += msgCodeLength
    val msgLength = message.copyOfRange(index, index + msgSizeLength).intTrimZeros()
    index += msgSizeLength
    val contentArray = ByteArray(msgLength)
    readWithSize(input, contentArray, msgLength)

    val messageData = MessageData(this, msgCode, msgLength, contentArray.asList(), index + msgLength)
    LoggerWrapper.debug(this::class) { "Received: ${client.localAddress} from ${client.inetAddress}. Message '${messageData.message}'. Content as String: '${messageData.getContentAsString()}'" }

    return messageData
}

private fun readWithSize(
    input: InputStream,
    headerBuffer: ByteArray,
    length: Int
) {
    if (length == 0) {
        return
    }
    var read = 0
    do {
        val blockRead = input.read(headerBuffer, 0, length - read)
        if (blockRead == -1) throw RuntimeException("End of stream")
        read += blockRead
    } while (read != length)
}

fun RemoteClient.getMessage(
    message: ByteArray,
    offset: Int
): MessageData {
    var index = offset
    val msgSizeLength =
        (properties[MSG_SIZE_LENGTH] ?: throw RuntimeException("Property $MSG_SIZE_LENGTH not set")).toInt()
    val msgCodeLength =
        (properties[MSG_MESSAGE_LENGTH] ?: throw RuntimeException("Property $MSG_MESSAGE_LENGTH not set")).toInt()
    val msgCode = message.copyOfRange(index, index + msgCodeLength).stringTrimZeros()
    index = index.plus(msgCodeLength)
    val msgLength = message.copyOfRange(index, index + msgSizeLength).intTrimZeros()

    index = index.plus(msgSizeLength)
    val content = message.copyOfRange(index, index + msgLength)
    index = index.plus(msgLength)
    val messageData = MessageData(this, msgCode, msgLength, content.asList(), index - offset)
    LoggerWrapper.debug(this::class) { "Received: ${client.localAddress} from ${client.inetAddress}. Message '${messageData.message}'. Content as String: '${messageData.getContentAsString()}'" }
    return messageData
}

fun ByteArray.stringTrimZeros() = String(this).trim { it.toInt() == 0 }

fun ByteArray.intTrimZeros() = this.stringTrimZeros().toInt()

data class MessageData(
    val client: RemoteClient,
    val message: String,
    val length: Int,
    val content: List<Byte>,
    val totalLength: Int
) {
    fun getContentAsBytes(): ByteArray {
        return content.toByteArray()
    }

    fun getContentAsString(): String {
        String()
        return String(getContentAsBytes())
    }

    inline fun <reified T> deserialize(): T where T : MessageEntity {
        return MessageFactory.deserialize(getContentAsString())
    }
}

fun RemoteClient.flush() {
    val output = outputStream()
    output.flush()
}

public fun RemoteClient.inputStream() =
    this.client.getInputStream() ?: throw RuntimeException("Can't obtain input stream")

public fun RemoteClient.outputStream() =
    this.client.getOutputStream() ?: throw RuntimeException("Can't obtain output stream")