package me.kacper.barszczewski.common

enum class ServerListenerType {
    STARTED, DOWN
}

enum class ClientListenerType {
    INITIALIZED, CLOSED, FILE_TRANSFER
}

enum class ClientServerListenerType {
    NEW
}

enum class RemoteClientListenerType {
    NEW, CONNECTED, DISCONNECTED
}

interface ServerStatusListener {
    fun onServerUp()
    fun onServerDown()
}

interface ClientListener {
    fun onClientConnected(client: RemoteServerClient)
}

interface ClientStartupListener {
    fun onConnectionInitialized(client: RemoteServerClient)
}

interface RemoteServerClientCallback<T> {
    fun onConnectionEnded(client: T)
}