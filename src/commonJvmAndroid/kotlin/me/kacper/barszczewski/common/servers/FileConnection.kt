package me.kacper.barszczewski.common.servers

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.kacper.barszczewski.common.*
import me.kacper.barszczewski.common.entities.*
import me.kacper.barszczewski.common.utils.LoggerWrapper
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketTimeoutException
import java.util.*
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

enum class FileServerListenerType {
    STARTING, ENDED
}

enum class FileTransferType {
    SERVER_TO_CLIENT, CLIENT_TO_SERVER
}

enum class ExceptionTransferType {
    FILE_EXISTS
}

open class NullFileOutputStream(message: String = "File output stream is null") : Exception(message)
open class NullFileInputStream(message: String = "File input stream is null") : Exception(message)
open class FileTransferStatusException(status: FileTransferStatus, message: FileStatusMessage?): Exception("File satus: $status")

enum class FileTransferStatus {
    GOOD, // Override policy set, ready to be transferred
    SKIP, // Skip file
    TRANSFER, // File is being transferred
    FINISHED, // File was successfully transferred
    INTERRUPTED // Some error, remove file
}

enum class FileStatusMessage {
    MISSING_READ_PERMISSION,
    MISSING_WRITE_PERMISSION,
    REMOVING_OPERATION_FAILED,
    INTERNAL_ERROR,
    WRONG_LENGTH
}

enum class FileExistsReturnType {
    SKIP, SKIP_ALL, OVERRIDE, OVERRIDE_ALL, CANCEL;

    fun isSkip(): Boolean = this == SKIP || this == SKIP_ALL

    fun isOverride(): Boolean = this == OVERRIDE || this == OVERRIDE_ALL
}

interface ClientFileTransferListener {
    fun onTransferStart(fileClient: FileClient) {}

    fun onFileTransferred(fileClient: FileClient, fileTransferEntity: FileTransferEntity) {}

    fun onFileBeingTransferred(fileClient: FileClient, fileTransferEntity: FileTransferEntity) {}

    fun onTransferComplete(fileClient: FileClient) {}

    fun onFileTransferException(exception: Exception) {}

    fun handleException(fileClient: FileClient?, e: Exception) {}
}

interface ClientFileTransferHandler {

    fun handleFileExistsException(transferEntity: FileTransferEntity): FileExistsReturnType

    fun removeFile(transferEntity: FileTransferEntity): Boolean = false

    fun getFileOutputStream(transferEntity: FileTransferEntity): OutputStream? = null

    fun getFileInputStream(transferEntity: FileTransferEntity): InputStream? = null

    fun readFileLength(transferEntity: FileTransferEntity): Long = -1L

    fun isFileExisting(transferEntity: FileTransferEntity): Boolean = false

    fun canReadFile(transferEntity: FileTransferEntity): Boolean  = false

    fun canWriteFile(transferEntity: FileTransferEntity): Boolean  = false

    fun createFile(transferEntity: FileTransferEntity): Boolean = false
}

data class FileTransferInfo(
    val fileList: List<File>,
    val transferType: FileTransferType,
    val destFile: String
) {

    fun getDestination(): String {
        if (destFile.isBlank()) {
            return System.getProperty("user.home") ?: ""
        }
        return destFile
    }

}

open class FileServer(
    serverSocket: ServerSocket,
    private val fileInfo: FileTransferInfo,
    private val chunkRead: Int = 2048,
    private val soTimeout: Int = 10 * 1000
) : RemoteServerAbstract<FileClient, ServerListenerType>(serverSocket, "File server") {

    open val fileListeners: MutableMap<FileServerListenerType, MutableList<(FileServer) -> Unit>> = mutableMapOf()

    private val fileStack = Stack<File>()
    private val atomicIndex = AtomicLong()

    private val lock: ReentrantLock = ReentrantLock()
    private val condition: Condition = lock.newCondition()

    override fun run() {
        serverSocket.soTimeout = soTimeout
        LoggerWrapper.debug(this::class) { "Waiting for client to connect..." }
        val client = try {
            FileClient(nextClient(), serverProperties = SERVER_PROPERTIES.toMutableMap())
        } catch (e: SocketTimeoutException) {
            LoggerWrapper.debug(this::class) { "FileServer socket timeout after ${soTimeout}ms, closing..." }
            close()
            return
        }
        client.startIfShould()
        clients.add(client)

        LoggerWrapper.debug(this::class) { "Starting file transferring" }

        val fileList = if (fileInfo.transferType == FileTransferType.CLIENT_TO_SERVER) {
            fileInfo.fileList.map { File(fileInfo.getDestination(), it.name) }
        } else {
            fileInfo.fileList
        }
        fileStack.addAll(fileList.reversed())

        client.addTransferListener(object: ClientFileTransferListener {
            override fun onFileTransferred(fileClient: FileClient, fileTransferEntity: FileTransferEntity) {
                initializeNextFileTransfer(client)
            }

            override fun onTransferComplete(fileClient: FileClient) {
                close()
                LoggerWrapper.debug(this::class) { "FileServer closed" }
                lock.withLock { condition.signal() }
            }
        })
        client.addTransferHandler(getFileClientTransferHandler(fileList.toList()))
        initializeNextFileTransfer(client)

        lock.withLock { condition.await() }
    }

    private fun getFileClientTransferHandler(fileList: List<File>): ClientFileTransferHandler = object : ClientFileTransferHandler {
        override fun handleFileExistsException(transferEntity: FileTransferEntity): FileExistsReturnType {
            TODO("Not yet implemented")
        }

        override fun removeFile(transferEntity: FileTransferEntity): Boolean {
            return fileList[transferEntity.index.toInt()].delete()
        }

        override fun getFileInputStream(transferEntity: FileTransferEntity): InputStream {
            return fileList[transferEntity.index.toInt()].inputStream()
        }

        override fun getFileOutputStream(transferEntity: FileTransferEntity): OutputStream {
            return fileList[transferEntity.index.toInt()].outputStream()
        }

        override fun readFileLength(transferEntity: FileTransferEntity): Long {
            return fileList[transferEntity.index.toInt()].length()
        }

        override fun isFileExisting(transferEntity: FileTransferEntity): Boolean {
            return fileList[transferEntity.index.toInt()].exists()
        }

        override fun canReadFile(transferEntity: FileTransferEntity): Boolean {
            return fileList[transferEntity.index.toInt()].canRead()
        }

        override fun canWriteFile(transferEntity: FileTransferEntity): Boolean {
            return fileList[transferEntity.index.toInt()].canWrite()
        }

        override fun createFile(transferEntity: FileTransferEntity): Boolean {
            return fileList[transferEntity.index.toInt()].createNewFile()
        }
    }

    protected fun initializeNextFileTransfer(client: FileClient) {
        try {
            var file: File
            var index: Long
            synchronized(fileStack) {
                if (fileStack.isEmpty()) {
                    client.close()
                    return
                }
                file = fileStack.pop()
                index = atomicIndex.getAndAdd(1)
            }
            sendFileTransferMessage(client, file, index)
        } catch (e: Exception) {
            LoggerWrapper.error(this::class, e) { "File transfer Error" }
        }
    }

    protected open fun sendFileTransferMessage(client: FileClient, file: File, index: Long) {
        var fileLength = -1L
        if (fileInfo.transferType == FileTransferType.SERVER_TO_CLIENT) {
            fileLength = file.length()
        }
        client.sendFileTransferMessage(FileTransferEntity(file.toString(), fileLength, fileInfo.transferType, index))
    }

//    protected open fun readFile(client: FileClient, file: File, index: Int) {
//        val saveFile = File(fileInfo.destFile, file.name)
//        client.readFile(file, saveFile, chunkRead, index)
//        logger.debug { "File '$file' has been read" }
//    }
//
//    protected open fun sendFile(client: FileClient, file: File, index: Int) {
//        val destFile = File(fileInfo.getDestination(), file.name).canonicalPath
//        client.sendFile(file, destFile, chunkRead, index)
//    }

    open fun addFileStartingListener(listener: (FileServer) -> Unit) = addListener(FileServerListenerType.STARTING, listener)
    open fun addFileEndedListener(listener: (FileServer) -> Unit) = addListener(FileServerListenerType.ENDED, listener)

    @Suppress("UNCHECKED_CAST")
    open fun addListener(type: FileServerListenerType, listener: (FileServer) -> Unit) =
        fileListeners.getOrPut(type) { mutableListOf() }
            .add(listener as (RemoteServerAbstract<FileClient, ServerListenerType>) -> Unit)
}

open class RemoteFileClient(
    socket: Socket,
    serverProperties: MutableMap<String, String>
) : FileClient(
    socket,
    threadName = "Remote File Client",
    serverProperties = serverProperties
) {

    override fun handleFileStatusChange(fileStatus: FileStatusEntity) {
        filesStatus[fileStatus.index]?.let {
            it.fileStatus = fileStatus.status
            when (fileStatus.status) {
                FileTransferStatus.GOOD -> {
                    checkFile(it)
                }
                FileTransferStatus.TRANSFER -> {
                    if (!validateBeforeTransfer(it)) return
                    if (it.transferEntityInformation.transferType == FileTransferType.SERVER_TO_CLIENT) {
                        if (!isFileExisting(it.transferEntityInformation) && !createFile(it)) {
                            skipFile(it.index)
                            return
                        }
                    }
                    try {
                        setFileStream(it)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        skipFile(it.index)
                        return
                    }
                    if (it.transferEntityInformation.transferType == FileTransferType.CLIENT_TO_SERVER) {
                        launchChunkTransfer(it)
                    } else {
                        sendMessage(FileStatusEntity(FileTransferStatus.TRANSFER, it.index))
                    }
                }
                else -> {
                    super.handleFileStatusChange(fileStatus)
                }
            }
        }
    }

    override fun checkFile(transferInfo: FileTransferInfo) {
        val sendGoodMessage =
            if (transferInfo.transferEntityInformation.transferType == FileTransferType.CLIENT_TO_SERVER) {
                checkFileReadAccess(transferInfo)
            } else {
                checkOverridePolicy(transferInfo.transferEntityInformation)
            }
        if (sendGoodMessage) {
            sendMessage(FileStatusEntity(FileTransferStatus.GOOD, transferInfo.index))
        }
    }

    override fun handleFileInformationTransfer(fileTransferEntity: FileTransferEntity) {
        filesStatus[fileTransferEntity.index] = FileTransferInfo(fileTransferEntity, "")
        clientFileTransferListeners.forEach { it.onFileBeingTransferred(this, fileTransferEntity) }
        if (fileTransferEntity.transferType == FileTransferType.SERVER_TO_CLIENT && fileTransferEntity.fileLength < 0) {
            // Server is sending file, but not provided its length
            skipFile(fileTransferEntity.index)
            return
        }
        if (fileTransferEntity.transferType == FileTransferType.CLIENT_TO_SERVER) {
            fileTransferEntity.fileLength = clientFileTransferHandler?.readFileLength(fileTransferEntity) ?: -1
            if (fileTransferEntity.fileLength < 0) {
                LoggerWrapper.warn(this::class) { "$clientFileTransferHandler" }
                LoggerWrapper.warn(this::class) { "File ${fileTransferEntity.filePath} have -1 length" }
                skipFile(fileTransferEntity.index)
                return
            }
        }
        sendMessage(fileTransferEntity)
    }

    override fun checkOverridePolicy(fileTransferEntity: FileTransferEntity): Boolean {
        val fileExists = isFileExisting(fileTransferEntity)
        if (fileExists && transferPolicy == FileExistsReturnType.SKIP_ALL) {
            skipFile(fileTransferEntity.index)
            return false
        }
        if (fileExists && transferPolicy == FileExistsReturnType.OVERRIDE_ALL) {
            filesStatus[fileTransferEntity.index]?.let {
                it.fileExistsPolicy = FileExistsReturnType.OVERRIDE_ALL
            }
//            sendMessage(FileStatusEntity(FileTransferStatus.GOOD, fileTransferEntity.index))
            return true
        }
        if (fileExists) {
//            handleFileOverriding(fileTransfer)
//            sendMessage(FileOverridePolicyEntity(fileTransferEntity.filePath, index = fileTransferEntity.index))
            handleFilePolicyChange(FileOverridePolicyEntity(
                    fileTransferEntity.filePath,
                    clientFileTransferHandler?.handleFileExistsException(fileTransferEntity),
                    fileTransferEntity.index
            ))
            return false
        }
//        sendMessage(FileStatusEntity(FileTransferStatus.GOOD, fileTransferEntity.index))
        return true
    }

    override fun setFileStream(it: FileTransferInfo) {
        clientFileTransferHandler?.let { handler ->
            when (it.getTransferType()) {
                FileTransferType.SERVER_TO_CLIENT -> {
                    val outputStream = handler.getFileOutputStream(it.transferEntityInformation) ?: throw NullFileOutputStream()
                    it.setOutputStream(outputStream)
                }
                FileTransferType.CLIENT_TO_SERVER -> {
                    val inputStream = handler.getFileInputStream(it.transferEntityInformation) ?: throw NullFileInputStream()
                    it.setInputStream(inputStream)
                }
            }
        }
    }

    override fun handleFilePolicyChange(fileOverridePolicyEntity: FileOverridePolicyEntity) {
        var overridePolicy: FileExistsReturnType? = null
        filesStatus[fileOverridePolicyEntity.index]?.let {
            if (it.getTransferType() == FileTransferType.CLIENT_TO_SERVER) {
                overridePolicy = clientFileTransferHandler?.handleFileExistsException(it.transferEntityInformation)
            }
        }
        if (overridePolicy != null) {
            fileOverridePolicyEntity.fileExistsPolicy = overridePolicy
            sendMessage(fileOverridePolicyEntity)
        } else {
            super.handleFilePolicyChange(fileOverridePolicyEntity)
        }
    }

}

//open class FileClient(
//    client: Socket,
//    private val fileEntity: FileEntity,
//    private val destDic: File,
//    private val shouldOverride: Boolean = false,
//    private val chuckRead: Int = 2048,
//    override var properties: MutableMap<String, String>
//) : FileClientAbstract(client, threadName = "File Client") {

    //    private val fileListeners: MutableMap<FileClientListenerType, MutableList<(FileClient) -> Unit>> = mutableMapOf()
//    protected var clientFileTransferListener: ClientFileTransferListener? = null
//    protected var clientFileTransferHandler: ClientFileTransferHandler? = null
//
//    init {
//        addMessageHandler(Message.FILE_TRANSFER) { transferFile(it.deserialize()) }
//    }
//
//    override fun run() {
//        logger.debug { "Connected to File Server with Ip ${client.remoteSocketAddress}" }
//
//        clientFileTransferListener?.onTransferStart(this)

//        try {
//            handleMessages()
//        } catch (e: Exception) {
//            clientFileTransferListener?.handleException(fileClient, e)
//            sendMessage(MSG_SKIP)
//        }
//        for (i in 0..fileEntity.filesCount) {
//            transferFile()
//        }
//        clientFileTransferListener?.onTransferComplete(this)
//
//        close()
//    }

//    protected open fun transferFile(fileTransferEntity: FileTransferEntity) {
//        clientFileTransferListener?.onFileBeingTransferred(this, fileTransferEntity)
//        try {
//            when (fileTransferEntity.transferType) {
//                FileTransferType.SERVER_TO_CLIENT -> readAndSaveFile(fileTransferEntity)
//                FileTransferType.CLIENT_TO_SERVER -> readAndSendFile(fileTransferEntity)
//            }
//        } catch (e: Exception) {
//            clientFileTransferListener?.onFileTransferError(e)
//            sendMessage(MSG_SKIP)
//        }
//        clientFileTransferListener?.onFileTransferred(this, fileTransferEntity)
//    }

//    protected open fun readAndSendFile(fileTransferEntity: FileTransferEntity) {
//        invokeListeners(FileClientListenerType.STARTING)
//
//        val file = File(fileTransferEntity.filePath)
//
//        fileTransferEntity.fileLength = clientFileTransferListener?.readFileLength(fileTransferEntity) ?: file.length()
//
//        sendMessage(fileTransferEntity)
//        var startReading = false
//        do {
//            val message = readMessage()
//            if (message.message == MSG_SKIP) {
//                logger.debug { "File '${file.canonicalPath}' has been skipped" }
//                return
//            }
//            if (message.message == Message.FILE_OVERR_POLICY) {
//                val filePolicyEntity: FileOverridePolicyEntity = message.deserialize()
//                filePolicyEntity.fileExistsPolicy = (overridePolicy ?: getOverridePolicy(fileTransferEntity))
//                sendMessage(filePolicyEntity)
//            }
//            if (message.message == MSG_IS_GOOD) {
//                startReading = true
//            }
//        } while (!startReading)
//
//        val inputStream = clientFileTransferListener?.readFile(fileTransferEntity) ?: file.inputStream()
//
//        writeStreamFile(outputStream(), inputStream, fileTransferEntity, chuckRead)
//        inputStream.close()
//        logger.debug { "File '${file.canonicalPath}' has been sent" }
//        invokeListeners(FileClientListenerType.TRANSFERRED)
//    }

//    protected open fun readAndSaveFile(fileTransferEntity: FileTransferEntity) {
//        checkOverridePolicy(fileTransferEntity)
//
//        if (shouldSkip) {
//            sendMessage(MSG_SKIP)
//            return
//        }
//        invokeListeners(FileClientListenerType.STARTING)
//        sendMessage(MSG_IS_GOOD)
//
//        val outputStream = clientFileTransferListener?.saveFile(fileTransferEntity) ?: Files.newOutputStream(Paths.get(fileTransferEntity.filePath))
//        writeStreamFile(outputStream, inputStream(), fileTransferEntity, chuckRead)
//        logger.debug { "File '${fileTransferEntity.filePath}' has been read" }
//        outputStream.flush()
//        outputStream.close()
//        invokeListeners(FileClientListenerType.TRANSFERRED)
//    }

//    protected open fun prepare(fileTransfer: FileTransfer): File {
//        val destFile = File(fileTransfer.filePath)
//        if (destFile.exists()) {
//            handleFileOverriding(fileTransfer)
//        }
//        if (!destFile.exists() && !destFile.createNewFile()) {
//            throw RuntimeException("Can't create '${destFile.canonicalPath}' file")
//        }
//
//        if (!destFile.canWrite()) {
//            throw RuntimeException("Cannot write into file '${destFile.canonicalPath}'")
//        }
//        return destFile
//    }
//
//    protected open fun handleFileOverriding(fileTransfer: FileTransfer) {
//        val policy = overridePolicy ?: clientFileTransferHandler?.handleFileExistsException(fileTransfer)
//        logger.debug { "Override situation, resolving with policy $policy" }
//        when (policy) {
//            FileExistsReturnType.OVERRIDE -> Files.delete(Paths.get(fileTransfer.filePath))
//            FileExistsReturnType.OVERRIDE_ALL -> {
//                Files.delete(Paths.get(fileTransfer.filePath))
//                overridePolicy = FileExistsReturnType.OVERRIDE_ALL
//            }
//            FileExistsReturnType.SKIP -> shouldSkip = true
//            FileExistsReturnType.SKIP_ALL -> {
//                shouldSkip = true
//                overridePolicy = FileExistsReturnType.SKIP_ALL
//            }
//            FileExistsReturnType.CANCEL -> {
//                throw TransferClosedByClient("Client determinate to cancel transfer when override exception detected")
//            }
//        }
//    }

//    override fun getOverridePolicy(fileTransferEntity: FileTransferEntity): FileExistsReturnType {
//        return clientFileTransferHandler?.handleFileExistsException(fileTransferEntity) ?: FileExistsReturnType.SKIP
//    }
//
//    override fun close() {
//        closed = true
//        this.client.close()
//        thread.interrupt()
//    }
//
//    open fun addTransferListener(clientFileTransferListener: ClientFileTransferListener) {
//        this.clientFileTransferListener = clientFileTransferListener
//    }
//
//    open fun addTransferExceptionHandler(clientFileTransferHandler: ClientFileTransferHandler) {
//        this.clientFileTransferHandler = clientFileTransferHandler
//    }


//    protected fun invokeListeners(type: FileClientListenerType) {
//        fileListeners.getOrPut(type) { mutableListOf() }.forEach { it.invoke(this) }
//    }

//    fun addFileStartingListener(listener: (FileClient) -> Unit) = addListener(FileClientListenerType.STARTING, listener)
//    fun addFileTransferredListener(listener: (FileClient) -> Unit) =
//        addListener(FileClientListenerType.TRANSFERRED, listener)

//    fun addListener(type: FileClientListenerType, listener: (FileClient) -> Unit) =
//        fileListeners.getOrPut(type) { mutableListOf() }
//            .add(listener)

//}

@Suppress("MemberVisibilityCanBePrivate")
open class FileClient(
    client: Socket,
    threadName: String = "FileClient",
    startThread: Boolean = true,
    serverProperties: MutableMap<String, String> = mutableMapOf()
) : RemoteClient(client, threadName = threadName, startThread = startThread) {

    protected var clientFileTransferListeners: MutableList<ClientFileTransferListener> = mutableListOf()
    protected var clientFileTransferHandler: ClientFileTransferHandler? = null
        set (value){
            lock.withLock {
                field = value
                condition.signalAll()
            }
        }

    protected val filesStatus = mutableMapOf<Long, FileTransferInfo>()

    var transferPolicy: FileExistsReturnType? = null

    /**
     * Lock and condition used for #clientFileTransferHandler set
     * Client should not handle messages until handler has been set those variables
     * makes sure of it
     */
    private val lock: ReentrantLock = ReentrantLock()
    private val condition: Condition = lock.newCondition()

    init {
        properties = serverProperties
        addMessageHandler(Message.FILE_TRANSFER) { handleFileInformationTransfer(it.deserialize()) }
        addMessageHandler(Message.FILE_OVERR_POLICY) { handleFilePolicyChange(it.deserialize()) }
        addMessageHandler(Message.FILE_STATUS) { handleFileStatusChange(it.deserialize()) }
        addMessageHandler(Message.FILE_CHUNK) { handleFileChunk(it.deserialize()) }
//        if (this@FileClient.startThread) {
//            thread.start()
//        }
    }

    override fun run() {
        clientFileTransferListeners.forEach { it.onTransferStart(this) }
        waitForFileTransferHandler()
        super.run()
        clientFileTransferListeners.forEach { it.onTransferComplete(this) }
    }

    protected fun waitForFileTransferHandler() {
        if (clientFileTransferHandler == null) {
            lock.withLock {
                if (clientFileTransferHandler == null) {
                    LoggerWrapper.warn(this::class) { "Waiting for file handler..." }
                    condition.await()
                    LoggerWrapper.warn(this::class) { "Handler has been set!" }
                }
            }
        }
    }

    fun sendFileTransferMessage(fileTransferEntity: FileTransferEntity) {
        filesStatus[fileTransferEntity.index] = FileTransferInfo(fileTransferEntity, "")
        sendMessage(fileTransferEntity)
        clientFileTransferListeners.forEach { it.onFileBeingTransferred(this, fileTransferEntity) }
    }

    private fun handleFileChunk(fileChunk: FileChunkEntity) {
        filesStatus[fileChunk.index]?.let {
            if (it.isFileTransferred()) {
                finishFileTransfer(it.index).invoke()
                sendMessage(FileStatusEntity(FileTransferStatus.FINISHED, it.index))
            }
            if (it.fileStatus != FileTransferStatus.TRANSFER) {
                LoggerWrapper.warn(this::class) { "File chunk received but file transfer status is not TRANSFER" }
                return
            }
            it.writeChunk(fileChunk)
            LoggerWrapper.verbose(this::class) { "Written ${it.writtenDataLength}/${it.transferEntityInformation.fileLength} of file ${it.transferEntityInformation.filePath}" }
            if (it.isFileTransferred()) {
                finishFileTransfer(it.index).invoke()
                sendMessage(FileStatusEntity(FileTransferStatus.FINISHED, it.index))
            }
        }
    }

    protected open fun handleFileStatusChange(fileStatus: FileStatusEntity) {
        var runAfter: () -> Unit = {}
        filesStatus[fileStatus.index]?.let { it ->
            it.fileStatus = fileStatus.status
            when (fileStatus.status) {
                FileTransferStatus.GOOD -> {
                    startFileTransfer(it)
                }
                FileTransferStatus.FINISHED -> {
                    it.safeClose()
                    runAfter = finishFileTransfer(it.index)
                }
                FileTransferStatus.INTERRUPTED, FileTransferStatus.SKIP -> {
                    removeFileIfCreated(it)
                    runAfter = finishFileTransfer(it.index)
                    clientFileTransferListeners.forEach { lis -> lis.onFileTransferException(FileTransferStatusException(fileStatus.status, fileStatus.messageCode)) }
                }
                FileTransferStatus.TRANSFER -> {
                    if (validateBeforeTransfer(it)) {
                        launchChunkTransfer(it)
                    }
                }
            }
        }
        runAfter.invoke()
    }

    protected open fun launchChunkTransfer(it: FileTransferInfo) =
        GlobalScope.launch {
            try {
                writeStreamFile(
                    it.getInputStream(),
                    it.transferEntityInformation,
                    2048,
                    it.index
                )
            } catch (e: Exception) {
                e.printStackTrace()
                sendMessage(FileStatusEntity(FileTransferStatus.INTERRUPTED, it.index))
                removeFileIfCreated(it)
                finishFileTransfer(it.index).invoke()
            }
        }

    private fun removeFileIfCreated(it: FileTransferInfo) {
        if (it.fileCreated) {
            removeFile(it.transferEntityInformation)
        }
    }

    protected open fun startFileTransfer(it: FileTransferInfo) {
        if (it.getTransferType() == FileTransferType.CLIENT_TO_SERVER) {
            if (isFileExisting(it.transferEntityInformation)) {
                if (it.fileExistsPolicy?.isOverride() == true && !removeFile(it.transferEntityInformation)) {
                    skipFile(it.index, FileStatusMessage.REMOVING_OPERATION_FAILED)
                    return
                }
            } else {
                if (!createFile(it)) {
                    skipFile(it.index)
                    return
                }
            }
        }
        try {
            setFileStream(it)
        } catch (e: Exception) {
            e.printStackTrace()
            skipFile(it.index)
            return
        }
        sendMessage(FileStatusEntity(FileTransferStatus.TRANSFER, it.index))
        it.fileStatus = FileTransferStatus.TRANSFER
    }

    protected open fun isFileExisting(transferEntity: FileTransferEntity): Boolean {
        return clientFileTransferHandler?.isFileExisting(transferEntity) ?: false
    }

    protected open fun removeFile(transferEntity: FileTransferEntity): Boolean {
        return clientFileTransferHandler?.removeFile(transferEntity) ?: false
    }

    protected open fun createFile(transferInfo: FileTransferInfo): Boolean {
        val created = clientFileTransferHandler?.createFile(transferInfo.transferEntityInformation) ?: false
        transferInfo.fileCreated = created
        return created
    }

    protected open fun setFileStream(it: FileTransferInfo) {
        clientFileTransferHandler?.let { handler ->
            when (it.getTransferType()) {
                FileTransferType.CLIENT_TO_SERVER -> {
                    val outputStream = handler.getFileOutputStream(it.transferEntityInformation) ?: throw NullFileOutputStream()
                    it.setOutputStream(outputStream)
                }
                FileTransferType.SERVER_TO_CLIENT -> {
                    val inputStream = handler.getFileInputStream(it.transferEntityInformation) ?: throw NullFileInputStream()
                    it.setInputStream(inputStream)
                }
            }
        }
    }

    protected fun finishFileTransfer(index: Long): () -> Unit = {
        val fileTransferEntity = filesStatus.remove(index)
        fileTransferEntity?.let {
            it.safeClose()
            clientFileTransferListeners.forEach { ltn ->
                ltn.onFileTransferred(this, it.transferEntityInformation)
            }
        }
    }

    protected open fun handleFileInformationTransfer(fileTransferEntity: FileTransferEntity) {
        filesStatus[fileTransferEntity.index] = FileTransferInfo(fileTransferEntity, "")
        clientFileTransferListeners.forEach { it.onFileBeingTransferred(this, fileTransferEntity) }
        filesStatus[fileTransferEntity.index]?.let {
            it.transferEntityInformation = fileTransferEntity
            checkFile(it)
        }
    }

    //    As Server Client
    protected open fun checkFile(transferInfo: FileTransferInfo) {
        if (transferInfo.transferEntityInformation.transferType == FileTransferType.CLIENT_TO_SERVER) {
            if (checkOverridePolicy(transferInfo.transferEntityInformation) && checkFileWriteAccess(transferInfo)) {
                sendMessage(FileStatusEntity(FileTransferStatus.GOOD, transferInfo.index))
            }
        } else {
            if (checkFileReadAccess(transferInfo)) {
                sendMessage(FileStatusEntity(FileTransferStatus.GOOD, transferInfo.index))
            }
        }
    }

    protected open fun checkFileWriteAccess(transferInfo: FileTransferInfo): Boolean {
//        if (!transferInfo.getFile().canWrite()) {
        if (isFileExisting(transferInfo.transferEntityInformation) && clientFileTransferHandler?.canWriteFile(
                transferInfo.transferEntityInformation) == false
        ) {
            skipFile(transferInfo.index, FileStatusMessage.MISSING_WRITE_PERMISSION)
            return false
        }
        return true
    }

    protected open fun checkFileReadAccess(transferInfo: FileTransferInfo): Boolean {
//        if (!transferInfo.getFile().canRead()) {
        if (clientFileTransferHandler?.canReadFile(transferInfo.transferEntityInformation) == false) {
            skipFile(transferInfo.index, FileStatusMessage.MISSING_READ_PERMISSION)
            return false
        }
        return true
    }

    protected open fun handleFilePolicyChange(fileOverridePolicyEntity: FileOverridePolicyEntity) {
        fileOverridePolicyEntity.fileExistsPolicy?.let {
            if (it == FileExistsReturnType.OVERRIDE_ALL || it == FileExistsReturnType.SKIP_ALL) {
                transferPolicy = it
            }
        }
        filesStatus[fileOverridePolicyEntity.index]?.let {
            it.fileExistsPolicy = fileOverridePolicyEntity.fileExistsPolicy
            if (fileOverridePolicyEntity.fileExistsPolicy?.isSkip() == true) {
                skipFile(fileOverridePolicyEntity.index)
            } else {
                sendMessage(FileStatusEntity(FileTransferStatus.GOOD, fileOverridePolicyEntity.index))
            }
        }
    }

    protected open fun checkOverridePolicy(fileTransferEntity: FileTransferEntity): Boolean {
        val fileExists = isFileExisting(fileTransferEntity)
        if (fileExists && transferPolicy == FileExistsReturnType.SKIP_ALL) {
            skipFile(fileTransferEntity.index)
            return false
        }
        if (fileExists && transferPolicy == FileExistsReturnType.OVERRIDE_ALL) {
            filesStatus[fileTransferEntity.index]?.let {
                it.fileExistsPolicy = FileExistsReturnType.OVERRIDE_ALL
            }
//            sendMessage(FileStatusEntity(FileTransferStatus.GOOD, fileTransferEntity.index))
            return true
        }
//        shouldSkip = shouldSkip && overridePolicy == FileExistsReturnType.SKIP_ALL
//        val destFile = File(fileTransferEntity.filePath)
        if (fileExists) {
//            handleFileOverriding(fileTransfer)
            sendMessage(FileOverridePolicyEntity(fileTransferEntity.filePath, index = fileTransferEntity.index))
            return false
        }
//        sendMessage(FileStatusEntity(FileTransferStatus.GOOD, fileTransferEntity.index))
        return true
//        if (shouldSkip) {
//            return
//        }
//        if (!destFile.exists() && !destFile.createNewFile()) {
//            throw RuntimeException("Can't create '${destFile.canonicalPath}' file")
//        }
//
//        if (!destFile.canWrite()) {
//            throw RuntimeException("Cannot write into file '${destFile.canonicalPath}'")
//        }
    }

    protected fun skipFile(index: Long, statusMessage: FileStatusMessage? = null) {
        clientFileTransferListeners.forEach { it.onFileTransferException(FileTransferStatusException(FileTransferStatus.SKIP, statusMessage)) }
        sendMessage(FileStatusEntity(FileTransferStatus.SKIP, index, statusMessage))
        filesStatus[index]?.safeClose()
        finishFileTransfer(index).invoke()
    }

//    fun handleFileOverriding(fileTransferEntity: FileTransferEntity) {
//        val policy = this.overridePolicy ?: getOverridePolicy(fileTransferEntity)
//        when (policy) {
//            FileExistsReturnType.OVERRIDE -> Files.delete(Paths.get(fileTransfer.filePath))
//            FileExistsReturnType.OVERRIDE_ALL -> {
//                Files.delete(Paths.get(fileTransfer.filePath))
//                overridePolicy = FileExistsReturnType.OVERRIDE_ALL
//            }
//            FileExistsReturnType.SKIP -> shouldSkip = true
//            FileExistsReturnType.SKIP_ALL -> {
//                shouldSkip = true
//                overridePolicy = FileExistsReturnType.SKIP_ALL
//            }
//            FileExistsReturnType.CANCEL -> {
//                throw TransferClosedByClient("Client determinate to cancel transfer when override exception detected")
//            }
//        }
//    }

//    open fun getOverridePolicy(fileTransferEntity: FileTransferEntity): FileExistsReturnType {
//        sendMessage(FileOverridePolicyEntity(fileTransferEntity.filePath))
//        var message: FileOverridePolicyEntity? = null
//        do {
//            val messageData = readMessage()
//            if (messageData.message == Message.FILE_OVERR_POLICY) {
//                message = messageData.deserialize()
//            }
//        } while (message == null)
//        return message?.fileExistsPolicy ?: FileExistsReturnType.SKIP
//    }

    override fun handleException(e: Exception, messageData: MessageData) {
        val entity = messageData.deserialize<MessageEntity>()
        if (entity is IndexableFileEntity) {
            skipFile(entity.index, FileStatusMessage.INTERNAL_ERROR)
        }
    }

    protected open fun validateBeforeTransfer(it: FileTransferInfo): Boolean {
        if (it.transferEntityInformation.fileLength < 0) {
            skipFile(it.index, FileStatusMessage.WRONG_LENGTH)
            return false
        }
        return true
    }

    open fun addTransferListener(clientFileTransferListener: ClientFileTransferListener) {
        this.clientFileTransferListeners.add(clientFileTransferListener)
    }

    open fun addTransferHandler(clientFileTransferHandler: ClientFileTransferHandler) {
        this.clientFileTransferHandler = clientFileTransferHandler
    }

    inner class FileTransferInfo(
        var transferEntityInformation: FileTransferEntity,
        var onDevicePath: String,
    ) {
        var index = transferEntityInformation.index
        var fileStatus: FileTransferStatus? = null
        var fileExistsPolicy: FileExistsReturnType? = null
        var writtenDataLength: Long = 0L
        var fileCreated: Boolean = false

        private lateinit var file: File
        private lateinit var outputStream: OutputStream
        private lateinit var inputStream: InputStream

        fun getTransferType(): FileTransferType = transferEntityInformation.transferType


        fun setOutputStream(outputStream: OutputStream) {
            if(this::outputStream.isInitialized) {
               return
            }
            this.outputStream = outputStream
        }

        fun getOutputStream(): OutputStream {
            return outputStream
        }

        fun setInputStream(inputStream: InputStream) {
            if(this::inputStream.isInitialized) {
                return
            }
            this.inputStream = inputStream
        }

        fun getInputStream(): InputStream {
            return inputStream
        }

//        private fun getFile(): File {
//            if(!this::file.isInitialized) {
//                file = File(transferEntityInformation.filePath)
//            }
//            return file
//        }

        fun safeClose() {
            try {
                if (this::outputStream.isInitialized) {
                    outputStream.flush()
                    outputStream.close()
                }
                if (this::inputStream.isInitialized) {
                    inputStream.close()
                }
            } catch (e: Exception) {
                LoggerWrapper.error(this::class, e) { "Output close error" }
            }
        }

//        fun removeFile() {
//            if (!getFile().delete()) {
//                logger.error { "Can't delete file ${getFile()}" }
//            }
//        }

        fun writeChunk(fileChunk: FileChunkEntity) {
            getOutputStream().write(fileChunk.data)
            writtenDataLength += fileChunk.chunkLength
        }

        fun isFileTransferred(): Boolean = writtenDataLength == transferEntityInformation.fileLength
    }
}