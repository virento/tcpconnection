package me.kacper.barszczewski.common

import kotlinx.coroutines.*
import me.kacper.barszczewski.common.Message.FILE_CONNECTION
import me.kacper.barszczewski.common.Message.FILE_INITIALIZE
import me.kacper.barszczewski.common.entities.FileConnectionEntity
import me.kacper.barszczewski.common.entities.TransferInitializationEntity
import me.kacper.barszczewski.common.servers.FileClient
import me.kacper.barszczewski.common.servers.FileServer
import me.kacper.barszczewski.common.servers.FileTransferInfo
import me.kacper.barszczewski.common.servers.RemoteFileClient
import me.kacper.barszczewski.common.utils.LoggerWrapper
import java.io.File
import java.lang.Runnable
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.net.Socket
import java.net.SocketException
import java.util.regex.Pattern
import java.util.stream.Collectors
import kotlin.concurrent.thread

/**
 * MESSAGE;LONG - size and content
 */

const val MSG_SIZE_LENGTH: String = "MESSAGE_SIZE_LENGTH"
const val MSG_CONN_INITIALIZED: String = "CONN_INIT"
const val MSG_DELIMITER: String = "MESSAGE_DELIMITER"
const val MSG_MESSAGE_LENGTH: String = "MESSAGE_LENGTH"
const val MSG_DISCONNECTED: String = "MESSAGE_DISCONNECTED"

val SERVER_PROPERTIES = mapOf(
    MSG_SIZE_LENGTH to Long.SIZE_BYTES.toString(),
    MSG_DELIMITER to ";",
    MSG_MESSAGE_LENGTH to "24"
)

//inline fun <reified T> deserialize(): T where T : MessageEntity
abstract class RemoteServerAbstract<T : RemoteClient, N>(
    open val serverSocket: ServerSocket,
    threadName: String = "RemoteServerAbstract"
) : Runnable, RemoteServerClientCallback<T> {

    open val clients: MutableList<T> = mutableListOf()
    open val listeners: MutableMap<N, MutableList<(RemoteServerAbstract<T, N>) -> Unit>> = mutableMapOf()

    open var closed: Boolean = false
        protected set
    open val isThreadRunning: Boolean
        get() {
            return thread.isAlive && !thread.isInterrupted
        }

    /**
     * is true only, when thread actually started and was closed (main loop in run() has been finished -> thread coming to end)
     */
    open var isDown = false
        protected set

    protected open val thread = thread(
        name = threadName
    ) {
        run()
    }

    protected open fun nextClient(): Socket {
        val client = serverSocket.accept()
        LoggerWrapper.debug(this::class) { "New client with ip ${client.inetAddress}" }
        return client
    }

    open fun close() {
        clients.forEach {
            try {
                it.sendMessage(MSG_DISCONNECTED)
            } catch (e: SocketException) {
                LoggerWrapper.warn(this::class) { "Socket exception: ${e.message}" }
            } catch (e: Exception) {
                LoggerWrapper.warn(this::class) { "Can't send message to ${it.client.inetAddress}" }
            }
        }
        closed = true
        this.serverSocket.close()
    }

    protected open fun addListener(type: N, listener: (RemoteServerAbstract<T, N>) -> Unit) =
        listeners.getOrPut(type) { mutableListOf() }.add(listener)

    protected open fun invokeListeners(type: N) {
        listeners.getOrPut(type) { mutableListOf() }.forEach { it.invoke(this) }
    }

    override fun onConnectionEnded(client: T) {
        if (client.isThreadRunning) {
            throw RuntimeException("Trying to remove running client!")
        }
        LoggerWrapper.info(this::class) { "Removing ${client.client.inetAddress} from client list" }
        clients.remove(client)
    }

}

object SocketServerInstance {

    fun getSocket(port: Int): ServerSocket {
        val socketServer = try {
            ServerSocket(port)
        } catch (e: Exception) {
            LoggerWrapper.error(this::class, e) { "Can't create server on port $port" }
            throw e
        }
        LoggerWrapper.debug(this::class) { "Server running on port ${socketServer.localPort}" }
        return socketServer
    }
}

object RemoteConnection {

    fun <T> createServer(
        port: Int,
        creator: (ServerSocket) -> T,
        callback: (T) -> Unit = {}
    ): Deferred<T> {
//        val completableFuture = CompletableFuture<T>()
        return GlobalScope.async {
            val socket = SocketServerInstance.getSocket(port)
            val serverInstance = creator(socket)
            callback(serverInstance)
//            completableFuture.complete(serverInstance)
            serverInstance
        }
//        return completableFuture
    }

    fun createFileServer(
        fileInfo: FileTransferInfo,
        port: Int,
        chunkRead: Int = 2048,
        soTimeout: Int = 10 * 1000,
        callback: (FileServer) -> Unit = {}
    ): Deferred<FileServer> {
        return createServer(port, creator = { FileServer(it, fileInfo, chunkRead, soTimeout) }, callback)
    }


    fun createConnection(port: Int, callback: (RemoteServer) -> Unit = { }): Deferred<RemoteServer> {
        return createServer(port, creator = { RemoteServer(it) }, callback)
    }


    fun <T: RemoteClient> connectToServer(
        hostname: String,
        port: Int,
        creator: (Socket) -> T,
        callback: (T) -> Unit = {},
        errorCallback: () -> Unit = {}, //TODO: Add exception to method parameters
        connectionTimeout: Int = 3_000
    ): CompletableDeferred<T> {
        val completableFuture = CompletableDeferred<T>()
        GlobalScope.launch {
            kotlin.runCatching {
                val socket = Socket()
                socket.connect(InetSocketAddress(hostname, port), connectionTimeout)
                if (socket.isConnected) LoggerWrapper.debug(RemoteConnection::class) { "Successfully connected to server, input: ${!socket.isInputShutdown}, output: ${!socket.isOutputShutdown}" }
                else LoggerWrapper.warn(RemoteConnection::class) { "Connection failed" }
                val client = creator(socket)
                callback(client)
                client.startIfShould()
                completableFuture.complete(client)
            }.onFailure {
                it.printStackTrace()
                LoggerWrapper.warn(RemoteConnection::class) { "Connection failure" }
                errorCallback()
                completableFuture.completeExceptionally(it)
            }
        }
        return completableFuture
    }

    fun connectToServer(
        hostname: String,
        port: Int,
        callback: (RemoteClient) -> Unit,
        errorCallback: () -> Unit = { },
        startThread: Boolean = false,
        connectionTimeout: Int = 3_000
    ): CompletableDeferred<RemoteClient> {
        return connectToServer(hostname, port, creator = { RemoteClient(it, startThread) }, callback, errorCallback, connectionTimeout)
    }

}

open class RemoteServer(serverSocket: ServerSocket) :
    RemoteServerAbstract<RemoteClient, ServerListenerType>(serverSocket, "Remote server") {

    open protected val clientListeners: MutableMap<ClientServerListenerType, MutableList<(RemoteServerClient) -> Unit>> =
        mutableMapOf()

    override fun run() {
        try {
            invokeListeners(ServerListenerType.STARTED)

            while (!closed) {

                val client = nextClient()

                @Suppress("UNCHECKED_CAST")
                val remoteClient = RemoteServerClient(
                    client,
                    SERVER_PROPERTIES,
                    this as RemoteServerClientCallback<RemoteServerClient>
                )
                remoteClient.startIfShould()

                clients.add(remoteClient)

                invokeClientListeners(ClientServerListenerType.NEW, remoteClient)
            }
        } catch (e: InterruptedException) {
            if (closed) LoggerWrapper.debug(this::class) { "Closing remote connection..." }
            else e.printStackTrace()
        } catch (e: SocketException) {
            if (closed) LoggerWrapper.debug(this::class) { "Closing remote connection..." }
            else e.printStackTrace()
        } finally {
            isDown = true
        }
        invokeListeners(ServerListenerType.DOWN)
    }

    open fun addServerStartedListener(listener: (RemoteServer) -> Unit) = addServerListener(ServerListenerType.STARTED, listener)
    open fun addServerDownListener(listener: (RemoteServer) -> Unit) = addServerListener(ServerListenerType.DOWN, listener)

    @Suppress("UNCHECKED_CAST")
    open fun addServerListener(type: ServerListenerType, listener: (RemoteServer) -> Unit) =
        listeners.getOrPut(type) { mutableListOf() }
            .add(listener as (RemoteServerAbstract<RemoteClient, ServerListenerType>) -> Unit)

    protected open fun invokeClientListeners(type: ClientServerListenerType, client: RemoteServerClient) {
        clientListeners.getOrPut(type) { mutableListOf() }.forEach { it.invoke(client) }
    }
}

open class RemoteServerClient(
    client: Socket,
    serverProperties: Map<String, String>,
    val serverCallback: RemoteServerClientCallback<RemoteServerClient>
) : RemoteClient(client, threadName = "RemoteServerClient") {

    open var remoteListeners: MutableMap<RemoteClientListenerType, MutableList<(RemoteServerClient) -> Unit>> =
        mutableMapOf()

    init {
        properties = serverProperties.toMutableMap()
    }

    override fun addDisconnectMessageHandler() {
        addMessageHandler(MSG_DISCONNECTED) {
            LoggerWrapper.debug(this::class) { "Disconnecting from server..." }
            stop()
            serverCallback.onConnectionEnded(this)
        }
    }

    override fun addFileTransferInitializeMessageHandler() {
        val socketPort = 5000
        addMessageHandler(FILE_INITIALIZE) { messageData ->
            val fileEntity = messageData.deserialize<TransferInitializationEntity>()
            val fileList = fileEntity.filePath.map { path -> File(path) }.toList()
            RemoteConnection.createFileServer(
                FileTransferInfo(fileList, fileEntity.transferType, fileEntity.destPath),
                socketPort // TODO: Create some configuration
            ) {
                sendMessage(FileConnectionEntity(client.localAddress.canonicalHostName, socketPort))
            }
        }
    }

    override fun run() {
        invokeRemoteListeners(RemoteClientListenerType.NEW)

        sendInitializationMessages()
        sendProperty(MSG_CONN_INITIALIZED)

        invokeRemoteListeners(RemoteClientListenerType.CONNECTED)

        try {
            while (!closed) {
                handleMessage(*readMessages().toTypedArray())
            }
        } catch (e: InterruptedException) {
            if (closed) LoggerWrapper.debug(this::class) { "Closing remote client connection..." }
            else e.printStackTrace()
        } catch (e: InputClosedException) {
            LoggerWrapper.warn(this::class, e) { "Client disconnected, probably client closed socket" }
            stop()
        }

        invokeRemoteListeners(RemoteClientListenerType.DISCONNECTED)
    }

    open fun sendInitializationMessages() {
        sendProperty(MSG_SIZE_LENGTH to SERVER_PROPERTIES[MSG_SIZE_LENGTH])
        sendProperty(MSG_DELIMITER to SERVER_PROPERTIES[MSG_DELIMITER])
        sendProperty(MSG_MESSAGE_LENGTH to SERVER_PROPERTIES[MSG_MESSAGE_LENGTH])
    }

    open fun invokeRemoteListeners(type: RemoteClientListenerType) {
        remoteListeners.getOrPut(type) { mutableListOf() }.forEach { it.invoke(this) }
    }

    open fun addNewListener(listener: (RemoteServerClient) -> Unit) =
        addRemoteListener(RemoteClientListenerType.NEW, listener)

    open fun addConnectedListener(listener: (RemoteClient) -> Unit) =
        addRemoteListener(RemoteClientListenerType.CONNECTED, listener)

    open fun addDisconnectedListener(listener: (RemoteClient) -> Unit) =
        addRemoteListener(RemoteClientListenerType.DISCONNECTED, listener)

    open fun addRemoteListener(type: RemoteClientListenerType, listener: (RemoteServerClient) -> Unit) {
        remoteListeners.getOrPut(type) { mutableListOf() }.add(listener)
    }
}

/**
 * Instance of client whose connected to server
 */
open class RemoteClient(
    val client: Socket,
    val startThread: Boolean = true,
    threadName: String = "RemoteClient"
) : Runnable {

    var thread: Thread

    open var properties: MutableMap<String, String> = mutableMapOf()

    var messageHandler: MutableMap<String, MutableList<(MessageData) -> Unit>> = mutableMapOf()
    var listeners: MutableMap<ClientListenerType, MutableList<(RemoteClient) -> Unit>> = mutableMapOf()

    var closed = false
    var isInitialized = false
        protected set

    var fileClient: FileClient? = null

    open val isThreadRunning: Boolean
        get() {
            return thread.isAlive && !thread.isInterrupted
        }

    init {
        addDisconnectMessageHandler()
        addFileTransferInitializeMessageHandler()
        thread = thread(
            name = threadName,
            start = false
        ) {
            this.run()
        }
    }

    open fun startIfShould() {
        if (startThread) {
            thread.start()
        }
    }

    protected open fun addDisconnectMessageHandler() {
        addMessageHandler(MSG_DISCONNECTED) {
            LoggerWrapper.debug(this::class) { "Disconnecting from server..." }
            stop()
        }
    }

    protected open fun addFileTransferInitializeMessageHandler() {
        addMessageHandler(FILE_CONNECTION) {
            val deserialized = it.deserialize<FileConnectionEntity>()
            LoggerWrapper.debug(this::class) { "Creating file transfer client" }
            RemoteConnection.connectToServer(
                client.inetAddress.hostAddress, //TODO: Change
                deserialized.socketPort,
                creator = { client ->
                    val fileClient = RemoteFileClient(
                        client,
//                        true,
                        serverProperties = this.properties
                    )
                    fileClient
                },
                callback = { fileClient ->
                    LoggerWrapper.debug(this::class) { "Connected to file server ${fileClient.client}" }
                    this.fileClient = fileClient
                    invokeListeners(ClientListenerType.FILE_TRANSFER)
                }
            )
        }
    }

    override fun run() {
        try {
            readProperties()
            isInitialized = true
            invokeListeners(ClientListenerType.INITIALIZED)
        } catch (e: InterruptedException) {
            if (closed) LoggerWrapper.debug(this::class) { "Closing remote client connection..." }
            else e.printStackTrace()
        } catch (e: InputClosedException) {
            LoggerWrapper.warn(this::class, e) { "Client disconnected, probably client closed socket" }
            stop()
        } catch (e: SocketException) {
            LoggerWrapper.warn(this::class, e) { "Socket exception, probably client closed socket" }
            stop()
        }
        handleMessages()
    }

    protected open fun handleMessages() {
        try {
            while (!closed) {
                handleMessage(*readMessages().toTypedArray())
            }
        } catch (e: InterruptedException) {
            if (closed) LoggerWrapper.debug(this::class) { "Closing remote client connection..." }
            else e.printStackTrace()
        } catch (e: CloseClientWithException) {
            LoggerWrapper.debug(this::class, e) { "Soft closing remote client connection due to ${e.message}" }
        } catch (e: InputClosedException) {
            LoggerWrapper.warn(this::class, e) { "Client disconnected, probably client closed socket" }
            stop()
        } catch (e: SocketException) {
            if (!closed) {
                LoggerWrapper.error(this::class, e) { "Socket exception" }
            }
        } catch (e: Exception) {
            LoggerWrapper.error(this::class, e) { "Error while reading messages" }
        } finally {
            invokeListeners(ClientListenerType.CLOSED)
        }
    }

    protected open fun invokeListeners(type: ClientListenerType) {
        listeners.getOrPut(type) { mutableListOf() }.forEach { it.invoke(this) }
    }

    protected open fun readProperties() {
        if (properties.isNotEmpty()) {
            LoggerWrapper.debug(this::class) { "Properties has been provided by subclass" }
            return
        }
        var isEnded = false
        var messageByte = readAvailable()
        var endIndex = 0
        val pattern = Pattern.compile("""\;(?:(?!\;))+""")
        while (!isEnded) {
            var message = String(messageByte)
            var indexOf = message.indexOf(MSG_CONN_INITIALIZED)
            if (indexOf != -1) {
                isEnded = true
                endIndex = indexOf + MSG_CONN_INITIALIZED.length
            } else {
                indexOf = message.length
                messageByte = readAvailable()
            }
            val serverProperties = message.substring(0, indexOf).split(pattern).stream()
                .map { it.split(":") }
                .filter { it.size == 2 }
                .collect(Collectors.toMap({ it[0] }, { it[1] }))
            properties.putAll(serverProperties)
        }
        LoggerWrapper.debug(this::class) { "Properties: $properties" }
        if (endIndex < messageByte.size) {
            handleMessage(getMessage(messageByte, endIndex))
        }
    }

    fun addMessageHandler(message: String, handler: (MessageData) -> Unit) {
        messageHandler.getOrPut(message) { mutableListOf() }.add(handler)
    }

    protected open fun handleMessage(vararg messages: MessageData) {
        messages.forEach {
            LoggerWrapper.debug(this::class) { "Executed ${messageHandler[it.message]?.size ?: -1} listeners for message ${it.message}" }
            messageHandler.getOrPut(it.message) { mutableListOf() }.forEach { handler -> invokeHandler(handler, it) }
        }
    }

    protected open fun invokeHandler(handler: (MessageData) -> Unit, messageData: MessageData) {
        try {
            handler.invoke(messageData)
        } catch (e: Exception) {
            handleException(e, messageData)
        }
    }

    protected open fun handleException(e: Exception, messageData: MessageData) {
        LoggerWrapper.error(this::class, e) { "Error handling message: ${messageData.getContentAsString()}" }
    }

    /**
     * Close client (send message + close connection and thread)
     */
    open fun close() {
        if (isInitialized) {
            sendMessage(MSG_DISCONNECTED)
        }
        stop()
    }

    /**
     * Close connection and thread
     */
    open fun stop() {
        closed = true
        this.client.close()
        thread.interrupt()
    }

    open fun addClientInitializedListener(listener: (RemoteClient) -> Unit) =
        addListener(ClientListenerType.INITIALIZED, listener)

    open fun addClientClosedListener(listener: (RemoteClient) -> Unit) = addListener(ClientListenerType.CLOSED, listener)

    open fun addListener(type: ClientListenerType, listener: (RemoteClient) -> Unit) =
        listeners.getOrPut(type) { mutableListOf() }.add(listener)

}