package me.kacper.barszczewski.common

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass
import me.kacper.barszczewski.common.entities.*

object Message {

    const val FILE_CHUNK: String = "F_CHUNK"
    const val FILE_INITIALIZE = "F_TR_INITIALIZE"
    const val FILE_CONNECTION = "F_CONNECTION"
    const val FILE_TRANSFER = "F_TRANSFER"
    const val FILE_OVERR_POLICY = "F_OV_POLICY"
    const val FILE_STATUS = "F_STATUS"
    const val CONNECTION_INFO = "CONNECTION_INFO"

}

object MessageFactory {

    private val module = SerializersModule {
        polymorphic(MessageEntity::class) {
            subclass(FileTransferEntity::class)
            subclass(TransferInitializationEntity::class)
            subclass(FileConnectionEntity::class)
            subclass(FileOverridePolicyEntity::class)
            subclass(FileStatusEntity::class)
            subclass(FileChunkEntity::class)
        }
    }

    var jsonObject: Json = Json {ignoreUnknownKeys = true; serializersModule = module}

    fun serialize(entity: MessageEntity): String {
        return jsonObject.encodeToString(entity)
    }

    inline fun <reified T> deserialize(serializedEntity: String): T {
        return jsonObject.decodeFromString(serializedEntity)
    }

}