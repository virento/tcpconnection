package me.kacper.barszczewski.common.utils

import com.github.aakira.napier.Napier
import kotlin.reflect.KClass

class LoggerWrapper<E: Any>(
    private val clazz: KClass<E>?
) {
    fun info(message: () -> String) {
        Napier.i(buildMessage(clazz, message))
    }

    fun debug(message: () -> String) {
        Napier.d(buildMessage(clazz, message))
    }

    fun debug(exception: Exception, message: () -> String) {
        Napier.d(buildMessage(clazz, message), exception)
    }

    fun warn(exception: Exception, message: () -> String) {
        Napier.w(buildMessage(clazz, message), exception)
    }

    fun error(exception: Exception, message: () -> String) {
        Napier.e(buildMessage(clazz, message), exception)
    }

    fun error(message: () -> String) {
        Napier.e(buildMessage(clazz, message))
    }

    fun warn(message: () -> String) {
        Napier.w(buildMessage(clazz, message))
    }

    fun verbose(message: () -> String) {
        Napier.v(buildMessage(clazz, message))
    }

    companion object {

        private fun buildMessage(clazz: KClass<*>?, message: () -> String): String {
            val builder = StringBuilder()
            if (clazz != null) {
                builder.append(clazz.simpleName)
            }
            builder.append("|")
            builder.append(message())

            return builder.toString()
        }

        fun info(clazz: KClass<*>, message: () -> String) {
            Napier.i(buildMessage(clazz, message))
        }

        fun debug(clazz: KClass<*>, message: () -> String) {
            Napier.d(buildMessage(clazz, message))
        }

        fun debug(clazz: KClass<*>, exception: Exception, message: () -> String) {
            Napier.d(buildMessage(clazz, message), exception)
        }

        fun warn(clazz: KClass<*>, exception: Exception, message: () -> String) {
            Napier.w(buildMessage(clazz, message), exception)
        }

        fun error(clazz: KClass<*>, exception: Exception, message: () -> String) {
            Napier.e(buildMessage(clazz, message), exception)
        }

        fun error(clazz: KClass<*>, message: () -> String) {
            Napier.e(buildMessage(clazz, message))
        }

        fun warn(clazz: KClass<*>, message: () -> String) {
            Napier.w(buildMessage(clazz, message))
        }

        fun verbose(clazz: KClass<*>, message: () -> String) {
            Napier.v(buildMessage(clazz, message))
        }
    }

}