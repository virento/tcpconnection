package me.kacper.barszczewski.common

import com.github.aakira.napier.DebugAntilog
import com.github.aakira.napier.Napier

actual fun getPlatformName(): String {
    return "Android"
}

actual fun initializeLogging() {
    Napier.base(DebugAntilog())
}