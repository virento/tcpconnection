package me.kacper.barszczewski.common

import me.kacper.barszczewski.common.entities.FileTransferEntity
import me.kacper.barszczewski.common.servers.ClientFileTransferHandler
import me.kacper.barszczewski.common.servers.FileExistsReturnType
import me.kacper.barszczewski.common.utils.LoggerWrapper
import java.io.File
import java.io.InputStream
import java.io.OutputStream

class FileHandlerBuilder(val fileList: List<File>) {

    private val logger = LoggerWrapper(FileHandlerBuilder::class)

    var fileExistsException: (FileTransferEntity) -> FileExistsReturnType = { FileExistsReturnType.SKIP }

    var removeFile: (FileTransferEntity) -> Boolean = { fileList[it.index.toInt()].delete() }

    var fileOutputStream: (FileTransferEntity) -> OutputStream = { fileList[it.index.toInt()].outputStream() }

    var fileInputStream: (FileTransferEntity) -> InputStream = { fileList[it.index.toInt()].inputStream() }

    var fileLength: (FileTransferEntity) -> Long = { logger.debug { "Test ${it.filePath}" };fileList[it.index.toInt()].length() }

    var fileExisting: (FileTransferEntity) -> Boolean = { fileList[it.index.toInt()].exists() }

    var readFile: (FileTransferEntity) -> Boolean = { fileList[it.index.toInt()].canRead() }

    var writeFile: (FileTransferEntity) -> Boolean = { fileList[it.index.toInt()].canWrite() }

    var createFile: (FileTransferEntity) -> Boolean = { fileList[it.index.toInt()].createNewFile() }


    fun fileExistsException(function: (FileTransferEntity) -> FileExistsReturnType) =
        apply { fileExistsException = function }

    fun removeFile(function: (FileTransferEntity) -> Boolean) = apply { removeFile = function }
    fun fileOutputStream(function: (FileTransferEntity) -> OutputStream) = apply { fileOutputStream = function }
    fun fileInputStream(function: (FileTransferEntity) -> InputStream) = apply { fileInputStream = function }
    fun fileLength(function: (FileTransferEntity) -> Long) = apply { fileLength = function }
    fun fileExisting(function: (FileTransferEntity) -> Boolean) = apply { fileExisting = function }
    fun readFile(function: (FileTransferEntity) -> Boolean) = apply { readFile = function }
    fun writeFile(function: (FileTransferEntity) -> Boolean) = apply { writeFile = function }
    fun createFile(function: (FileTransferEntity) -> Boolean) = apply { createFile = function }

    fun build() = object : ClientFileTransferHandler {
        override fun handleFileExistsException(transferEntity: FileTransferEntity): FileExistsReturnType = fileExistsException.invoke(transferEntity)

        override fun removeFile(transferEntity: FileTransferEntity): Boolean = removeFile.invoke(transferEntity)

        override fun getFileOutputStream(transferEntity: FileTransferEntity): OutputStream = fileOutputStream.invoke(transferEntity)

        override fun getFileInputStream(transferEntity: FileTransferEntity): InputStream = fileInputStream.invoke(transferEntity)

        override fun readFileLength(transferEntity: FileTransferEntity): Long = fileLength.invoke(transferEntity)

        override fun isFileExisting(transferEntity: FileTransferEntity): Boolean = fileExisting.invoke(transferEntity)

        override fun canReadFile(transferEntity: FileTransferEntity): Boolean = readFile.invoke(transferEntity)

        override fun canWriteFile(transferEntity: FileTransferEntity): Boolean = writeFile.invoke(transferEntity)

        override fun createFile(transferEntity: FileTransferEntity): Boolean = createFile.invoke(transferEntity)
    }
}