package me.kacper.barszczewski.common

import kotlin.test.assertFails

fun assertFail(message: String) {
    assertFails { println(message) }
}
