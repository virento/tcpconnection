package me.kacper.barszczewski.common

import com.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import me.kacper.barszczewski.common.entities.FileTransferEntity
import me.kacper.barszczewski.common.entities.TransferInitializationEntity
import me.kacper.barszczewski.common.servers.*
import me.kacper.barszczewski.common.utils.LoggerWrapper
import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.test.assertEquals
import kotlin.test.assertTrue


@Suppress("LocalVariableName")
class SocketTest {

    private val logger = LoggerWrapper(SocketTest::class)

    data class Locker(
        val lock: ReentrantLock = ReentrantLock(),
        val condition: Condition = lock.newCondition(),
        var signalQueue: Int = 0
    ) {

        fun signalAfter(run: Runnable) {
            lock.withLock {
                run.run()
                signalQueue++
                condition.signalAll()
            }
        }

        fun await() {
            lock.withLock {
                if (signalQueue == 0) {
                    condition.await()
                }
                signalQueue--
            }
        }

        fun signal() {
            lock.withLock {
                signalQueue++
                condition.signalAll()
            }
        }

    }

    companion object {

        @BeforeClass
        @JvmStatic
        fun beforeTest() {
            Napier.base(ConsoleAntilog())
            println("#".repeat(50))
        }

        @AfterClass
        @JvmStatic
        fun afterTest() {
            println("#".repeat(50))
        }

        private val locks: MutableMap<Long, Locker> = mutableMapOf()

        private fun getLock(ID: Long): Locker = locks.getOrPut(ID) { Locker() }
        fun signalAfter(id: Long, run: Runnable) = getLock(id).signalAfter(run)
        fun await(id: Long) = getLock(id).await()
        fun signal(id: Long) = getLock(id).signal()

    }

    @ExperimentalCoroutinesApi
    @Test(timeout = 5000)
    fun serverStartUpTest() {
        val ID = 1L
        val PORT = 555 + ID

        val remoteConnection: RemoteServer = createServerConnectionAndReturn(PORT, ID)
        remoteConnection.addServerDownListener {
            signalAfter(ID) {
                logger.info { "Server closing..." }
                assertTrue { it.isDown }
            }
        }
        remoteConnection.close()
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
    }

    @Test(timeout = 5000)
    fun serverStartUpAndOneClientConnectTest() {
        val ID = 2L
        val PORT = 555 + ID

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteClient.close()
            Assert.assertTrue(remoteClient.closed)
            delay(1_000)
            remoteConnection.close()
            signalAfter(ID) {}
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
    }

    @Test(timeout = 5000)
    fun serverStartUpAndCloseOneClientConnectTest() {
        val ID = 3L
        val PORT = 555 + ID

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteConnection.close()
            delay(1_000)
            signalAfter(ID) {}
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
        assertTrue { !remoteClient.isThreadRunning }
    }

    @Test
    fun serverToClientOneFileTransfer() {
        val ID = 4L
        val PORT = 555 + ID

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        val dest = "transferTest/dest/test0/"
        val fileList = listOf("transferTest/src/file1.txt")
        val mappedClientFiles = fileList.map { File(dest, File(it).name) }

        remoteClient.addListener(ClientListenerType.FILE_TRANSFER) {
            it.fileClient?.addTransferListener(clientFileTransferListener(ID))
            it.fileClient?.addTransferHandler(clientFileTransferHandler(ID, mappedClientFiles))
        }

        val fileEntity = createFileEntity(
            FileTransferType.SERVER_TO_CLIENT,
            dest,
            *fileList.toTypedArray()
        )
        remoteClient.sendMessage(fileEntity)

        await(ID)
        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteConnection.close()
            delay(1_000)
            signalAfter(ID) {}
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
        assertTrue { !remoteClient.isThreadRunning }
        assertTrue { File("transferTest/dest/test0/file1.txt").exists() }
        assertTrue { File("transferTest/src/file1.txt").exists() }
        assertEquals(File("transferTest/dest/test0/file1.txt").length(), File("transferTest/src/file1.txt").length())
        Files.delete(Paths.get("transferTest/dest/test0/file1.txt"))
    }

    @Test
    fun serverToClientThreeFileTransfer() {
        val ID = 5L
        val PORT = 555 + ID

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        val fileList = listOf(
            "transferTest/src/file1.txt",
            "transferTest/src/test2/file2.txt",
            "transferTest/src/test2/test3/file3.txt"
        )
        val dest = "transferTest/dest/test1/"
        val mappedClientFiles = fileList.map { File(dest, File(it).name) }
        val fileTransferType = FileTransferType.SERVER_TO_CLIENT

        remoteClient.addListener(ClientListenerType.FILE_TRANSFER) {
            it.fileClient?.addTransferListener(clientFileTransferListener(ID))
            it.fileClient?.addTransferHandler(clientFileTransferHandler(ID, mappedClientFiles))
        }

        val fileEntity = createFileEntity(
            fileTransferType,
            dest,
            *fileList.toTypedArray()
        )
        remoteClient.sendMessage(fileEntity)

        await(ID)
        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteConnection.close()
            delay(1_000)
            signalAfter(ID) {}
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
        assertTrue { !remoteClient.isThreadRunning }
        assertTrue { File("transferTest/dest/test1/file1.txt").exists() }
        assertTrue { File("transferTest/dest/test1/file2.txt").exists() }
        assertTrue { File("transferTest/dest/test1/file3.txt").exists() }
        assertTrue { File("transferTest/src/file1.txt").exists() }
        assertTrue { File("transferTest/src/test2/file2.txt").exists() }
        assertTrue { File("transferTest/src/test2/test3/file3.txt").exists() }
        assertEquals(File("transferTest/src/file1.txt").length(), File("transferTest/dest/test1/file1.txt").length())
        assertEquals(
            File("transferTest/src/test2/file2.txt").length(),
            File("transferTest/dest/test1/file2.txt").length()
        )
        assertEquals(
            File("transferTest/src/test2/test3/file3.txt").length(),
            File("transferTest/dest/test1/file3.txt").length()
        )
        Files.delete(Paths.get("transferTest/dest/test1/file1.txt"))
        Files.delete(Paths.get("transferTest/dest/test1/file2.txt"))
        Files.delete(Paths.get("transferTest/dest/test1/file3.txt"))
    }

    @Test
    fun clientToServerThreeFileTransfer() {
        val ID = 6L
        val PORT = 555 + ID

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)


        val fileList = listOf(
            "transferTest/src/file1.txt",
            "transferTest/src/test2/file2.txt",
            "transferTest/src/test2/test3/file3.txt"
        )
        val dest = "transferTest/dest/test2"
        val mappedClientFiles = fileList.map { File(it) }
        val fileTransferType = FileTransferType.CLIENT_TO_SERVER
        remoteClient.addListener(ClientListenerType.FILE_TRANSFER) {
            it.fileClient?.addTransferListener(clientFileTransferListener(ID))
            it.fileClient?.addTransferHandler(clientFileTransferHandler(ID, mappedClientFiles))
        }

        val fileEntity = createFileEntity(
            fileTransferType,
            dest,
            *fileList.toTypedArray()
        )
        remoteClient.sendMessage(fileEntity)

        await(ID)
        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteConnection.close()
            delay(1_000)
            signal(ID)
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
        assertTrue { !remoteClient.isThreadRunning }
        assertTrue { File("transferTest/dest/test2/file1.txt").exists() }
        assertTrue { File("transferTest/dest/test2/file2.txt").exists() }
        assertTrue { File("transferTest/dest/test2/file3.txt").exists() }
        assertTrue { File("transferTest/src/file1.txt").exists() }
        assertTrue { File("transferTest/src/test2/file2.txt").exists() }
        assertTrue { File("transferTest/src/test2/test3/file3.txt").exists() }
        assertEquals(File("transferTest/src/file1.txt").length(), File("transferTest/dest/test2/file1.txt").length())
        assertEquals(
            File("transferTest/src/test2/file2.txt").length(),
            File("transferTest/dest/test2/file2.txt").length()
        )
        assertEquals(
            File("transferTest/src/test2/test3/file3.txt").length(),
            File("transferTest/dest/test2/file3.txt").length()
        )
        Files.delete(Paths.get("transferTest/dest/test2/file1.txt"))
        Files.delete(Paths.get("transferTest/dest/test2/file2.txt"))
        Files.delete(Paths.get("transferTest/dest/test2/file3.txt"))
    }

    @Test
    fun serverToClientFileExistsCaseOverrideFirstSkipRestTest() {
        val ID = 7L
        val PORT = 555 + ID

        val file1Content = "This should be override"
        val file2Content = "This should not be override"
        val file3Content = "This should not be override by third file"
        createFileWithText("transferTest/dest/test3/file1.txt", file1Content)
        createFileWithText("transferTest/dest/test3/file2.txt", file2Content)
        createFileWithText("transferTest/dest/test3/file3.txt", file3Content)

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        val fileList = listOf(
            "transferTest/src/file1.txt",
            "transferTest/src/test2/file2.txt",
            "transferTest/src/test2/test3/file3.txt"
        )
        val dest = "transferTest/dest/test3"
        val mappedClientFiles = fileList.map { File(dest, File(it).name) }
        remoteClient.addListener(ClientListenerType.FILE_TRANSFER) { client ->
            client.fileClient?.addTransferListener(clientFileTransferListener(ID))
            client.fileClient?.addTransferHandler(FileHandlerBuilder(mappedClientFiles)
                .fileExistsException {
                    if (it.filePath.endsWith("file1.txt")) FileExistsReturnType.OVERRIDE
                    else FileExistsReturnType.SKIP
                }
                .build()
            )
        }
        val fileEntity = createFileEntity(
            FileTransferType.SERVER_TO_CLIENT,
            dest,
            *fileList.toTypedArray()
        )
        remoteClient.sendMessage(fileEntity)

        await(ID)
        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteConnection.close()
            delay(1_000)
            signal(ID)
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
        assertTrue { !remoteClient.isThreadRunning }
        assertTrue { File("transferTest/dest/test3/file1.txt").exists() }
        assertTrue { File("transferTest/dest/test3/file2.txt").exists() }
        assertTrue { File("transferTest/dest/test3/file3.txt").exists() }
        assertTrue { File("transferTest/src/file1.txt").exists() }
        assertTrue { File("transferTest/src/test2/file2.txt").exists() }
        assertTrue { File("transferTest/src/test2/test3/file3.txt").exists() }
        assertEquals(File("transferTest/src/file1.txt").length(), File("transferTest/dest/test3/file1.txt").length())
        assertEquals(
            file2Content.length.toLong(),
            File("transferTest/dest/test3/file2.txt").length(),
        )
        assertEquals(
            file3Content.length.toLong(),
            File("transferTest/dest/test3/file3.txt").length(),
        )
        Files.delete(Paths.get("transferTest/dest/test3/file1.txt"))
        Files.delete(Paths.get("transferTest/dest/test3/file2.txt"))
        Files.delete(Paths.get("transferTest/dest/test3/file3.txt"))
    }

    @Test
    fun serverToClientFileExistsCaseOverrideAllTest() {
        val ID = 8L
        val PORT = 555 + ID

        val file1Content = "This should be override"
        val file2Content = "This should be override by second file"
        val file3Content = "This should be override by third file"
        createFileWithText("transferTest/dest/test3/file1.txt", file1Content)
        createFileWithText("transferTest/dest/test3/file2.txt", file2Content)
        createFileWithText("transferTest/dest/test3/file3.txt", file3Content)

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        var exceptionCounter = 0

        val fileList = listOf(
            "transferTest/src/file1.txt",
            "transferTest/src/test2/file2.txt",
            "transferTest/src/test2/test3/file3.txt"
        )
        val dest = "transferTest/dest/test3"
        val mappedClientFiles = fileList.map { File(dest, File(it).name) }
        remoteClient.addListener(ClientListenerType.FILE_TRANSFER) {
            it.fileClient?.addTransferListener(clientFileTransferListener(ID))
            it.fileClient?.addTransferHandler(FileHandlerBuilder(mappedClientFiles)
                .fileExistsException {
                    if (exceptionCounter > 0) {
                        Assert.fail("Method has been invoke more than once")
                    }
                    exceptionCounter++
                    FileExistsReturnType.OVERRIDE_ALL
                }
                .build()
            )
        }

        val fileEntity = createFileEntity(
            FileTransferType.SERVER_TO_CLIENT,
            dest,
            *fileList.toTypedArray()
        )
        remoteClient.sendMessage(fileEntity)

        await(ID)
        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteConnection.close()
            delay(1_000)
            signal(ID)
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
        assertTrue { !remoteClient.isThreadRunning }
        assertTrue { File("transferTest/dest/test3/file1.txt").exists() }
        assertTrue { File("transferTest/dest/test3/file2.txt").exists() }
        assertTrue { File("transferTest/dest/test3/file3.txt").exists() }
        assertTrue { File("transferTest/src/file1.txt").exists() }
        assertTrue { File("transferTest/src/test2/file2.txt").exists() }
        assertTrue { File("transferTest/src/test2/test3/file3.txt").exists() }
        assertEquals(File("transferTest/src/file1.txt").length(), File("transferTest/dest/test3/file1.txt").length())
        assertEquals(
            File("transferTest/src/test2/file2.txt").length(),
            File("transferTest/dest/test3/file2.txt").length(),
        )
        assertEquals(
            File("transferTest/src/test2/test3/file3.txt").length(),
            File("transferTest/dest/test3/file3.txt").length(),
        )
        Files.delete(Paths.get("transferTest/dest/test3/file1.txt"))
        Files.delete(Paths.get("transferTest/dest/test3/file2.txt"))
        Files.delete(Paths.get("transferTest/dest/test3/file3.txt"))
    }

    @Test
    fun serverToClientFileExistsCaseSkipAllTest() {
        val ID = 9L
        val PORT = 555 + ID

        val file1Content = "This should be override"
        val file2Content = "This should be override by second file"
        val file3Content = "This should be override by third file"
        createFileWithText("transferTest/dest/test3/file1.txt", file1Content)
        createFileWithText("transferTest/dest/test3/file2.txt", file2Content)
        createFileWithText("transferTest/dest/test3/file3.txt", file3Content)

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        var exceptionCounter = 0

        val fileList = listOf(
            "transferTest/src/file1.txt",
            "transferTest/src/test2/file2.txt",
            "transferTest/src/test2/test3/file3.txt"
        )
        val dest = "transferTest/dest/test3"
        val mappedClientFiles = fileList.map { File(dest, File(it).name) }
        remoteClient.addListener(ClientListenerType.FILE_TRANSFER) {
            it.fileClient?.addTransferListener(clientFileTransferListener(ID))
            it.fileClient?.addTransferHandler(FileHandlerBuilder(mappedClientFiles)
                .fileExistsException {
                    if (exceptionCounter > 0) {
                        Assert.fail("Method has been invoke more than once")
                    }
                    exceptionCounter++
                    FileExistsReturnType.SKIP_ALL
                }
                .build()
            )
        }

        val fileEntity = createFileEntity(
            FileTransferType.SERVER_TO_CLIENT,
            dest,
            *fileList.toTypedArray()
        )
        remoteClient.sendMessage(fileEntity)

        await(ID)
        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteConnection.close()
            delay(1_000)
            signal(ID)
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
        assertTrue { !remoteClient.isThreadRunning }
        assertTrue { remoteClient.fileClient != null }
        assertTrue { remoteClient.fileClient?.closed!! }
        assertTrue { !remoteClient.fileClient?.isThreadRunning!! }
        assertTrue { File("transferTest/dest/test3/file1.txt").exists() }
        assertTrue { File("transferTest/dest/test3/file2.txt").exists() }
        assertTrue { File("transferTest/dest/test3/file3.txt").exists() }
        assertTrue { File("transferTest/src/file1.txt").exists() }
        assertTrue { File("transferTest/src/test2/file2.txt").exists() }
        assertTrue { File("transferTest/src/test2/test3/file3.txt").exists() }
        assertEquals(file1Content.length.toLong(), File("transferTest/dest/test3/file1.txt").length())
        assertEquals(
            file2Content.length.toLong(),
            File("transferTest/dest/test3/file2.txt").length(),
        )
        assertEquals(
            file3Content.length.toLong(),
            File("transferTest/dest/test3/file3.txt").length(),
        )
        Files.delete(Paths.get("transferTest/dest/test3/file1.txt"))
        Files.delete(Paths.get("transferTest/dest/test3/file2.txt"))
        Files.delete(Paths.get("transferTest/dest/test3/file3.txt"))
    }

    @Test
    fun clientToServerFileExistsCaseOverrideOneTest() {
        val ID = 9L
        val PORT = 555 + ID

        val file1Content = "This should be override"
        val file2Content = "This should not be override"
        createFileWithText("transferTest/dest/test4/file1.txt", file1Content)
        createFileWithText("transferTest/dest/test4/file2.txt", file2Content)
        Thread.sleep(1000)

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        val fileList = listOf(
            "transferTest/src/file1.txt",
            "transferTest/src/test2/file2.txt",
            "transferTest/src/test2/test3/file3.txt"
        )
        val dest = "transferTest/dest/test4"
        val mappedClientFiles = fileList.map { File(it) }
        remoteClient.addListener(ClientListenerType.FILE_TRANSFER) { client ->
            client.fileClient?.addTransferListener(clientFileTransferListener(ID))
            client.fileClient?.addTransferHandler(FileHandlerBuilder(mappedClientFiles)
                .fileExistsException {
                    val file = File(it.filePath)
                    if (file.name.equals("file1.txt")) {
                        return@fileExistsException FileExistsReturnType.OVERRIDE
                    }
                    FileExistsReturnType.SKIP
                }.build()
            )
        }

        val fileEntity = createFileEntity(
            FileTransferType.CLIENT_TO_SERVER,
            dest,
            *fileList.toTypedArray()
        )
        remoteClient.sendMessage(fileEntity)

        await(ID)
        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteConnection.close()
            delay(1_000)
            signal(ID)
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
        assertTrue { !remoteClient.isThreadRunning }
        assertTrue { File("transferTest/dest/test4/file1.txt").exists() }
        assertTrue { File("transferTest/dest/test4/file2.txt").exists() }
        assertTrue { File("transferTest/dest/test4/file3.txt").exists() }
        assertTrue { File("transferTest/src/file1.txt").exists() }
        assertTrue { File("transferTest/src/test2/file2.txt").exists() }
        assertTrue { File("transferTest/src/test2/test3/file3.txt").exists() }
        assertEquals(File("transferTest/src/file1.txt").length(), File("transferTest/dest/test4/file1.txt").length())
        assertEquals(
            file2Content.length.toLong(),
            File("transferTest/dest/test4/file2.txt").length(),
        )
        assertEquals(
            File("transferTest/src/test2/test3/file3.txt").length(),
            File("transferTest/dest/test4/file3.txt").length(),
        )

        Files.delete(Paths.get("transferTest/dest/test4/file1.txt"))
        Files.delete(Paths.get("transferTest/dest/test4/file2.txt"))
        Files.delete(Paths.get("transferTest/dest/test4/file3.txt"))
    }

    @Test
    fun serverToClientFileTransferAccessErrorTest() {
        val ID = 10L
        val PORT = 555 + ID

        val remoteConnection = createServerConnectionAndReturn(PORT, ID)
        val remoteClient = connectToServerAndReturn(PORT, ID)

        var errorHappened = false


        val fileList = listOf(
            "transferTest/src/file1#!txt"
        )
        val dest = "transferTest/dest/test4"
        val mappedClientFiles = fileList.map { File(dest, File(it).name) }
        remoteClient.addListener(ClientListenerType.FILE_TRANSFER) {
            it.fileClient?.addTransferListener(object : ClientFileTransferListener {
                override fun onTransferStart(fileClient: FileClient) {
                    it.fileClient?.addTransferHandler(FileHandlerBuilder(mappedClientFiles).build())
                }

                override fun onFileTransferred(fileClient: FileClient, fileTransferEntity: FileTransferEntity) {  }

                override fun onFileBeingTransferred(fileClient: FileClient, fileTransferEntity: FileTransferEntity) {  }

                override fun onTransferComplete(fileClient: FileClient) { signal(ID) }

                override fun onFileTransferException(exception: Exception) { errorHappened = true; logger.error(exception){"Error"} }

            })
        }

        val fileEntity = createFileEntity(
            FileTransferType.SERVER_TO_CLIENT,
            dest,
            *fileList.toTypedArray()
        )
//        await(ID)
        remoteClient.sendMessage(fileEntity)

        await(ID)
        @Suppress("DeferredResultUnused")
        GlobalScope.async {
            delay(1_000)
            remoteConnection.close()
            delay(1_000)
            signalAfter(ID) {}
        }
        await(ID)
        assertTrue { !remoteConnection.isThreadRunning }
        assertTrue { remoteConnection.isDown }
        assertTrue { !remoteClient.isThreadRunning }
        assertTrue { errorHappened }
    }

    private fun createFileWithText(filePath: String, content: String) {
        val path = Paths.get(filePath)
        if (Files.exists(path)) Files.delete(path)
        Files.write(path, content.toByteArray(), StandardOpenOption.CREATE_NEW)
    }

    private fun clientFileTransferListener(ID: Long) = object : ClientFileTransferListener {
        override fun onTransferStart(fileClient: FileClient) {
            logger.info { "File transfer started" }
        }

        override fun onTransferComplete(fileClient: FileClient) {
            logger.info { "File transfer completed" }
            signalAfter(ID) {}
        }

        override fun onFileTransferException(exception: Exception) {
            logger.info { "File transfer error" }
        }

        override fun onFileTransferred(fileClient: FileClient, fileTransferEntity: FileTransferEntity) {
            logger.info { "File '${fileTransferEntity.filePath}' transferred" }
        }

        override fun onFileBeingTransferred(fileClient: FileClient, fileTransferEntity: FileTransferEntity) {
            logger.info { "File '${fileTransferEntity.filePath}' being transferred" }
        }
    }

    private fun clientFileTransferHandler(ID: Long, fileList: List<File>): ClientFileTransferHandler {
        return FileHandlerBuilder(fileList).build()
    }

    private fun createFileEntity(
        fileTransferType: FileTransferType,
        dest: String,
        vararg file: String
    ): TransferInitializationEntity {
        return TransferInitializationEntity(listOf(*file), fileTransferType, dest)
    }

    private fun connectToServerAndReturn(
        PORT: Long,
        ID: Long
    ): RemoteClient {
        var remoteClient: RemoteClient? = null
        RemoteConnection.connectToServer("localhost", PORT.toInt(), callback = {
            signalAfter(ID) {
//                println("Connected to server localhost:$PORT")
                assertTrue { !it.closed }
                remoteClient = it
            }
        }, startThread = true)
        await(ID)
        return remoteClient ?: throw RuntimeException("RemoteConnection is null")
    }

    @Suppress("DeferredResultUnused")
    private fun createServerConnectionAndReturn(
        PORT: Long,
        ID: Long
    ): RemoteServer {
        var remoteServer: RemoteServer? = null
        RemoteConnection.createConnection(PORT.toInt(), callback = {
            signalAfter(ID) {
//                println("Server started, signaling lock...")
                assertTrue { !it.serverSocket.isClosed }
                remoteServer = it
            }
        })
        await(ID)
        return remoteServer ?: throw RuntimeException("RemoteServer is null")
    }
}