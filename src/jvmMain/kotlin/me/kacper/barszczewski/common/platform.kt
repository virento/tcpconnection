package me.kacper.barszczewski.common

import com.github.aakira.napier.DebugAntilog
import com.github.aakira.napier.Napier

actual fun getPlatformName(): String {
    return "Desktop"
}

actual fun initializeLogging() {
    Napier.base(DebugAntilog())
}